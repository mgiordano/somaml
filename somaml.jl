#!/usr/bin/env julia

### somaml.jl --- Self-Organizing Map for Astrometric MicroLensing
# Copyright (C) 2016 Mosè Giordano.  License is MIT "Expat".

### Code:

# The function used to determine distance between input vector and weigth vector
distance{T<:AbstractFloat}(rx::T, wx::T, ry::T, wy::T) =
    abs2(rx - wx) + abs2(ry - wy)

function training{T<:AbstractFloat}(x::Array{T, 2}, y::Array{T, 2}, init::T,
                                    trainers::Int,
                                    neurons_row::Int, neurons_col::Int,
                                    nepochs::Int, components::Int=size(x, 1),
                                    narrays::Int=size(x, 2))
    @assert size(x) == size(y)
    @assert trainers <= narrays
    X = copy(x)
    Y = copy(y)
    # Initialize the neuron grid with random "neurons_row × neurons_col"
    # arrays each with "components" components
    neuron_grid = rand(T, 2, components, neurons_row, neurons_col)
    @inbounds for epoch in linspace(0.0, 1.0, nepochs)
        α = init*(1.0 - epoch) # Learning coefficient rate
        # We adopt Θ = exp(-r²/σ²) as neighbourhood function.  The following
        # variable is 1/σ² and it'll be plugged into Θ (calculated below), which
        # will be used to update the winning neuron, together with the learning
        # coefficient rate.
        invσsq = 1/abs2(epoch + 0.5*neurons_row*(1.0 - epoch))
        for j = 1:trainers
            # Find the winning neuron (BMU, Best Matching Unit)
            bmu_row::Int = bmu_col::Int = -1
            mindist::T = +Inf
            # For each neuron...
            for col = 1:neurons_col, row = 1:neurons_row
                dist::T = 0.0
                # ...evaluate the distance between this (row, col) neuron and the
                # (j) input vector...
                for k = 1:components
                    dist = dist +
                        distance(X[k, j], neuron_grid[1, k, row, col],
                                 Y[k, j], neuron_grid[2, k, row, col])
                end # k
                # If this distance is less than mindist, record neuron position
                # and distance
                if dist < mindist
                    mindist = dist
                    bmu_row = row
                    bmu_col = col
                end
            end # col, row
            # For this input vector, once the winning neuron has been found,
            # update the winning neuron (and its neighbours) components
            for col = 1:neurons_col, row = 1:neurons_row
                # αΘ is the product of the learning rate coefficient α (computed
                # above), and the neighbourhood kernel Θ
                rowtmp = abs(row - bmu_row)
                coltmp = abs(col - bmu_col)
                rowdist = min(rowtmp, neurons_row - rowtmp)
                coldist = min(coltmp, neurons_col - coltmp)
                αΘ = α*exp(-0.5*(abs2(rowdist) + abs2(coldist))*invσsq)
                for k = 1:components
                    tmp1 = neuron_grid[1, k, row, col]
                    tmp2 = neuron_grid[2, k, row, col]
                    neuron_grid[1, k, row, col] = tmp1 + αΘ*(X[k, j] - tmp1)
                    neuron_grid[2, k, row, col] = tmp2 + αΘ*(Y[k, j] - tmp2)
                end # k
            end # col, row
        end # j
    end # epoch
    return neuron_grid
end

function matchxy{T<:AbstractFloat}(x::Array{T, 2}, y::Array{T, 2},
                                   neuron_grid::Array{T, 4},
                                   components::Int=size(x, 1),
                                   narrays::Int=size(x, 2),
                                   neurons_row::Int=size(neuron_grid, 3),
                                   neurons_col::Int=size(neuron_grid, 4))
    @assert size(x) == size(y)
    @assert components == size(neuron_grid)[2]
    matchx = Vector{Int}(narrays)
    matchy = similar(matchx)
    @inbounds for j = 1:narrays
        bmu_row::Int = bmu_col::Int = -1
        mindist::T = +Inf
        for col = 1:neurons_col, row = 1:neurons_row
            dist = 0.0
            for k = 1:components
                dist = dist +
                    distance(x[k, j], neuron_grid[1, k, row, col],
                             y[k, j], neuron_grid[2, k, row, col])
            end # k
            if dist < mindist
                mindist = dist
                bmu_row = row
                bmu_col = col
            end
        end # col, row
        matchx[j] = bmu_col # x coordinate is the column number
        matchy[j] = bmu_row # y coordinate is the row number
    end # j
    return matchx, matchy
end

function frequencygrid{T<:Int}(matchx::Vector{T}, matchy::Vector{T},
                               neurons_row::T, neurons_col::T,
                               narrays::T=length(matchx))
    # Initialize the neuron frequency grid to 0
    @assert length(matchx) == length(matchy)
    nfreq = zeros(Int, neurons_row, neurons_col)
    @inbounds for col = 1:neurons_col, row = 1:neurons_row
        for j = 1:narrays
            # x coordinate is the column number, y coordinate is the row number
            if matchx[j] == col && matchy[j] == row
                nfreq[row, col] += 1
            end
        end # j
    end # col, row
    return nfreq
end

# Function used to compute distance inside "umatrix" function.
udistance{T<:AbstractFloat}(w1::T, w2::T) = abs(w1) + abs(w2)

function umatrix{T<:AbstractFloat}(x::Array{T, 2}, y::Array{T, 2},
                                   neuron_grid::Array{T, 4},
                                   components::Int=size(x, 1),
                                   neurons_row::Int=size(neuron_grid, 3),
                                   neurons_col::Int=size(neuron_grid, 4))
    @assert size(x) == size(y)
    @assert components == size(neuron_grid)[2]
    U = Array(T, neurons_row, neurons_col)
    Δ::Int = 1
    pixels_kernel = 2Δ+1 # The neuron is always at the centre
    @inbounds for col = 1:neurons_col, row = 1:neurons_row
        pixel_ref::T = 0.0
        for k = 1:components
            pixel_ref = pixel_ref +
                udistance(neuron_grid[1, k, row, col], neuron_grid[2, k, row, col])
        end
        count::T = 0.0
        for Δcol = -Δ:Δ, Δrow = -Δ:Δ
            colnew = col + Δcol
            rownew = row + Δrow
            if 1 <= colnew <= neurons_col && 1 <= rownew <= neurons_row
                r = rownew
                c = colnew
            else
                r = row
                c = col
            end
            pixvalue = 0.0
            for k = 1:components
                pixvalue = pixvalue +
                    udistance(neuron_grid[1, k, r, c], neuron_grid[2, k, r, c])
            end
            count = count + abs2((pixel_ref - pixvalue)/components)
        end
        U[row, col] = count/abs2(pixels_kernel)
    end
    return U
end

##################################### Main #####################################

const init = 0.05 # α value of the net
const neurons_row = 20 # Number of rows in neuron grid
const neurons_col = neurons_row # Number of columns in neuron grid
const nepochs = 501 # Number of epochs for training
const trainers = 250 # Number of training arrays among all input arrays

println("Reading data...")
x = readdlm("xarray.txt")'
y = readdlm("yarray.txt")'
# Normalize arrays and write them to file.
scale_factors = reshape(1./max(maximum(abs(x), 1), maximum(abs(y), 1)), size(x, 2))
println("Normalizing data...")
scale!(x, scale_factors)
scale!(y, scale_factors)
writedlm("normalized_x.dat", x)
writedlm("normalized_y.dat", y)
# Training
println("Training...")
neuron_grid = training(x, y, init, trainers, neurons_row, neurons_col, nepochs)
writedlm("neuron_grid.dat", neuron_grid)
writedlm("neuron_grid_size.dat", collect(size(neuron_grid)))
# neuron_grid = reshape(readdlm("neuron_grid.dat"), readdlm("neuron_grid_size.dat", Int)...)
# Final match
println("Matching...")
matchx, matchy = matchxy(x, y, neuron_grid)
# Neuron map frequency
println("Frequency grid...")
nfreq = frequencygrid(matchx, matchy, neurons_row, neurons_col)
# U-Matrix
println("U-Matrix...")
U = umatrix(x, y, neuron_grid)

function givme(row::Integer, col::Integer)
    # Find vectors among all input arrays that have (row, col) coordinates in
    # scatter plot.
    n = intersect(find(x -> x == row, matchx), find(y -> y == col, matchy))
    plot(overwrite_figure=false, title="Pixel ($row, $col)")
    for i in n; plot!(x[:,i], y[:,i]); end
    plot!()
end

# Plot results
using Plots
flag = readdlm("flag.dat");
# heatmap(nfreq, xlim=(0.5, neurons_row+0.5), ylim=(0.5, neurons_col+0.5),
#         xticks=1:neurons_col, yticks=1:neurons_row, title="Number Density")
heatmap(U, xlim=(0.5, neurons_row+0.5), ylim=(0.5, neurons_col+0.5),
        xticks=1:neurons_col, yticks=1:neurons_row, title="U-Matrix")
scatter!(matchx, matchy, m=(:auto))
# scatter!(matchx[find(x->x==2, flag)], matchy[find(x->x==2, flag)],
#          lab="ellipses 2", m=(:auto))
# scatter!(matchx[find(x->x==0, flag)], matchy[find(x->x==0, flag)],
#          lab="circles", m=(:auto))
